Data is loaded from File.

![mesh.jpg](https://bitbucket.org/repo/jgkz4no/images/2348377196-mesh.jpg)


Main view.

![mesh1.jpg](https://bitbucket.org/repo/jgkz4no/images/1763557765-mesh1.jpg)

You can choose mesh from combobox 

![mesh2.jpg](https://bitbucket.org/repo/jgkz4no/images/2353923240-mesh2.jpg)

In next step is created a mash and display a data.

![mesh3.jpg](https://bitbucket.org/repo/jgkz4no/images/4138070093-mesh3.jpg)

In the end the calculated temperatures on node are displayed

![mesh4.jpg](https://bitbucket.org/repo/jgkz4no/images/2085041211-mesh4.jpg)