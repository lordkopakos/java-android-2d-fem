package com.example.lordkopakos.mes.IO;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by lordkopakos on 2017-02-11.
 */
public class LoadFile extends MyFile {
    public LoadFile(){
        super();
    }
    public LoadFile(String filePath, String fileName,  String fileEnlargement){
        super(filePath, fileName, fileEnlargement);
    }
    public void loadFile() throws IOException {
        FileInputStream fis = openFileInput(getFilePath()+getFileName()+getFileEnlargement());
        BufferedInputStream bis= new BufferedInputStream(fis);
        StringBuffer sb=new StringBuffer();

        while(bis.available()!=0){
            char c=(char)bis.read();
            sb.append(c);
        }

        bis.close();
        fis.close();
    }
}
