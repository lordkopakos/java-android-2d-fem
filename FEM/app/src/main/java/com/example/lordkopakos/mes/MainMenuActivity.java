package com.example.lordkopakos.mes;

import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import java.io.IOException;
import java.util.ArrayList;

public class MainMenuActivity extends AppCompatActivity {

    private String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        setSpinnerItems();
    }

    public void goToMeshActivity(View view){
        Spinner spinner = (Spinner)findViewById(R.id.spSelectMesh);
        try {
            String fileName = spinner.getSelectedItem().toString();
            String file = filePath + ";" + fileName + ";";
            Intent intent = new Intent(MainMenuActivity.this, MeshInformationActivity.class);
            intent.putExtra("file", file);
            startActivity(intent);
        }
        catch (NullPointerException npe){

        }

    }

    public void changeTypeMesh(View view){
        setSpinnerItems();
    }

    public void setSpinnerItems(){

        setFilePath();

        Spinner spinner=(Spinner)findViewById(R.id.spSelectMesh);

        ArrayList<String> items = new ArrayList<>();
        AssetManager assetManager = getApplicationContext().getAssets();
        try {
            for (String file : assetManager.list(filePath)) {
                if (file.endsWith(".txt"))
                    items.add(file.replaceAll(".txt$", ""));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this,   android.R.layout.simple_spinner_item, items);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner.setAdapter(spinnerArrayAdapter);

    }
    private void setFilePath(){

        ToggleButton tb=(ToggleButton)findViewById(R.id.tbtnTypeMesh);
        boolean onCreatedMeshes=tb.isChecked();
        if(onCreatedMeshes){
            filePath= "createdMeshes";
        }
        else{
            filePath= "exemplaryMeshes";
        }
    }

}
