package com.example.lordkopakos.mes.mesh;

import com.example.lordkopakos.mes.math.Math;

/**
 * Created by lordkopakos on 2017-02-12.
 */
public class Mesh {

    int globalNumberOfNodes;
    int globalNumberOfElements;
    Element elements[];

    //Macierze globalne
    float GH[][];
    float GP[];
    float GT[];

    //stałe
    static float ambientT;
    double eps = 1e-12;

    public Mesh(int globalNumberOfNodes, int globalNumberOfElements, int localNumberOfNodes){
        setGlobalNumberOfNodes(globalNumberOfNodes);
        setGlobalNumberOfElements(globalNumberOfElements);
        createAllElements(localNumberOfNodes);
        createGlobalMatrixH();
        createGlobalVectorP();
        createGlobalVectorT();
    }

    public Element getElement(int index) {
        return elements[index];
    }

    public float getTemperature(int index) {
        return GT[index];
    }

    public int getGlobalNumberOfElements() {
        return globalNumberOfElements;
    }

    public int getGlobalNumberOfNodes() {
        return globalNumberOfNodes;
    }

    public void setGlobalNumberOfNodes(int globalNumberOfNodes) {
        this.globalNumberOfNodes = globalNumberOfNodes;
    }

    public void setGlobalNumberOfElements(int globalNumberOfElements) {
        this.globalNumberOfElements = globalNumberOfElements;
    }

    public static void setAmbientT(float ambientT) {
        Mesh.ambientT = ambientT;
    }

    private void createAllElements(int localNumberOfNodes){
        this.elements=new Element[globalNumberOfElements];
        for(int elementIndex=0; elementIndex < globalNumberOfElements; elementIndex++){
            this.elements[elementIndex]=new Element(localNumberOfNodes, elementIndex);
        }
    }

    private void createGlobalMatrixH(){
        this.GH=new float[globalNumberOfNodes][globalNumberOfNodes];
        this.GH= Math.resetMatrix(globalNumberOfNodes, globalNumberOfNodes);
    }

    private void createGlobalVectorP(){
        this.GP=new float[globalNumberOfNodes];
        this.GP=Math.resetVector(globalNumberOfNodes);
    }

    private void createGlobalVectorT(){
        this.GT=new float[globalNumberOfNodes];
        this.GT=Math.resetVector(globalNumberOfNodes);
    }

    public static float getAmbientT() {
        return ambientT;
    }

    public void calculateMesh(){
        initElement();
        aggregationLocalToGlobalMatrixH();
        aggregationLocalToGlobalVectorP();
        calculateGlobalVectorT();
    }

    private void initElement(){
        for(int elementIndex=0; elementIndex < globalNumberOfElements ; elementIndex++){
            elements[elementIndex].initElement();
        }
    }
    private void aggregationLocalToGlobalMatrixH(){
        this.GH= Math.resetMatrix(globalNumberOfNodes, globalNumberOfNodes);
        for(int elementIndex=0; elementIndex < globalNumberOfElements;elementIndex++){
            addElementToGlobalMatrixH(elementIndex);
        }
    }

    private void addElementToGlobalMatrixH(int elementIndex){
        int localNumberOfNodes=elements[elementIndex].getNumberOfNodes();
        for(int localMatrixWidth=0; localMatrixWidth<localNumberOfNodes; localMatrixWidth++){
            for(int localMatrixHeight=0; localMatrixHeight < localNumberOfNodes ; localMatrixHeight++){
                int firstNodeId=elements[elementIndex].getNodes(localMatrixWidth).getIdNode();
                int secondNodeId=elements[elementIndex].getNodes(localMatrixHeight).getIdNode();
                GH[firstNodeId][secondNodeId]=elements[elementIndex].getLHValue(localMatrixWidth,localMatrixHeight);
            }
        }
    }

    private void aggregationLocalToGlobalVectorP(){
        this.GP=Math.resetVector(globalNumberOfNodes);
        for(int elementIndex=0; elementIndex < globalNumberOfElements;elementIndex++){
            addElementToGlobalVectorP(elementIndex);
        }
    }

    private void addElementToGlobalVectorP(int elementIndex){
        int localNumberOfNodes=elements[elementIndex].getNumberOfNodes();
        for(int localVectorWidth=0; localVectorWidth<localNumberOfNodes; localVectorWidth++){
            int nodeId=elements[elementIndex].getNodes(localVectorWidth).getIdNode();
            GP[nodeId]=elements[elementIndex].getLPValue(localVectorWidth);
        }
    }

    private void calculateGlobalVectorT(){
        float AB[][]=new float[globalNumberOfNodes][globalNumberOfNodes+1];


        for (int i = 0; i < globalNumberOfNodes; i++) {
            for (int k = 0; k < globalNumberOfNodes; k++) {
                AB[i][k] = GH[i][k];
            }
        }
        for (int i = 0; i < globalNumberOfNodes; i++) {
            AB[i][globalNumberOfNodes] = -GP[i];
        }

        gaussMethod(AB);
    }

    private boolean gaussMethod(float AB[][]){
        int i, j, k;
        float m, s;

        for (i = 0; i < globalNumberOfNodes - 1; i++)
        {
            for (j = i + 1; j < globalNumberOfNodes; j++)
            {
                if (java.lang.Math.abs(AB[i][i]) < eps) return false;
                m = -AB[j][i] / AB[i][i];
                for (k = i + 1; k <= globalNumberOfNodes; k++)
                    AB[j][k] += m * AB[i][k];
            }
        }

        for (i = globalNumberOfNodes - 1; i >= 0; i--)
        {
            s = AB[i][globalNumberOfNodes];
            for (j = globalNumberOfNodes - 1; j >= i + 1; j--)
                s -= AB[i][j] * GT[j];
            if (java.lang.Math.abs(AB[i][i]) < eps) return false;
            GT[i] = s / AB[i][i];
        }

        return true;
    }

}
