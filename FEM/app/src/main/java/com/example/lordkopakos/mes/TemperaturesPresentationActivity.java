package com.example.lordkopakos.mes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class TemperaturesPresentationActivity extends AppCompatActivity {

    ArrayList<String> temperaturesItems;
    BarChart temperaturesChart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperatures_presentation);
        Pattern semicolon= Pattern.compile(";");
        String[] temperatures=semicolon.split(getIntent().getExtras().getString("temperatures"));

        GridView gvTemperatures=(GridView)findViewById(R.id.gvTemperatures);
        gvTemperatures.setNumColumns(2);
        temperaturesItems = new ArrayList<>();
        addHeaderToETemperatures();

        temperaturesChart=(BarChart)findViewById(R.id.chtTemperature);
        ArrayList<BarEntry> barEntries= new ArrayList<>();
        for(int nodeindex=0;nodeindex<temperatures.length;nodeindex++){
            temperaturesItems.add(Integer.toString(nodeindex));
            temperaturesItems.add(temperatures[nodeindex]);
            float temperature= Float.valueOf(temperatures[nodeindex]);
            barEntries.add(new BarEntry(nodeindex,temperature));
        }


        ArrayAdapter<String> temperaturesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, temperaturesItems);
        gvTemperatures.setAdapter(temperaturesAdapter);

        BarDataSet dataSet = new BarDataSet(barEntries, "Temperatures");
        BarData barData= new BarData(dataSet);
        temperaturesChart.setData(barData);
    }

    private void addHeaderToETemperatures(){
        temperaturesItems.add("ID");
        temperaturesItems.add("Temperature");
    }
}
