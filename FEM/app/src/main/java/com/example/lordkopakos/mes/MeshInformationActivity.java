package com.example.lordkopakos.mes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.example.lordkopakos.mes.mesh.Mesh;
import com.example.lordkopakos.mes.mesh.Node;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class MeshInformationActivity extends AppCompatActivity {

    String filePath;
    String fileName;
    Mesh mesh;
    ArrayList<String> elementItems;
    ArrayList<String> nodesItems;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mesh_information);
        TextView meshTitle=(TextView)findViewById(R.id.tvMeshName);

        Pattern semicolon= Pattern.compile(";");
        String[] file=semicolon.split(getIntent().getExtras().getString("file"));
        filePath=file[0];
        fileName=file[1];
        meshTitle.setText(fileName);
        readFile();

        GridView gvElementsInformation=(GridView)findViewById(R.id.gvElementsInformation);
        gvElementsInformation.setNumColumns(6);
        elementItems = new ArrayList<>();
        addHeaderToElementsInformation();

        GridView gvNodesInformation=(GridView)findViewById(R.id.gvNodesInformation);
        gvNodesInformation.setNumColumns(4);
        nodesItems = new ArrayList<>();
        addHeaderToNodesInformation();

        int numberOfElements=mesh.getGlobalNumberOfElements();
        for(int elementIndex=0; elementIndex<numberOfElements; elementIndex++){
            mesh.getElement(elementIndex).initElement();
            setElementInformation(elementIndex);
        }
        ArrayAdapter<String> elementsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, elementItems);
        ArrayAdapter<String> nodesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nodesItems);
        gvElementsInformation.setAdapter(elementsAdapter);
        gvNodesInformation.setAdapter(nodesAdapter);

    }

    public void readFile(){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open(filePath+"/"+fileName +".txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            Pattern semicolon= Pattern.compile(";");
            int elementIndex=0;
            int endNode=0;
            while ((mLine = reader.readLine()) != null) {
                String[] arrayLine=semicolon.split(mLine);
                if(arrayLine[0].equals("#")){

                }
                else if(arrayLine[0].equals("ne")){
                    TextView tvNGE=(TextView)findViewById(R.id.tvNGE);
                    tvNGE.setText(arrayLine[1]);
                }
                else if(arrayLine[0].equals( "nn")){
                    TextView tvNGN=(TextView)findViewById(R.id.tvNGN);
                    TextView tvNLN=(TextView)findViewById(R.id.tvNLN);
                    tvNGN.setText(arrayLine[1]);
                    tvNLN.setText(arrayLine[2]);
                }
                else if(arrayLine[0].equals("at")){
                    TextView tvNGE=(TextView)findViewById(R.id.tvNGE);
                    TextView tvNGN=(TextView)findViewById(R.id.tvNGN);
                    TextView tvNLN=(TextView)findViewById(R.id.tvNLN);
                    TextView tvAmbientT=(TextView)findViewById(R.id.tvAmbientT);
                    tvAmbientT.setText(arrayLine[1]);
                    int numberOfGlobalElements=Integer.valueOf(tvNGE.getText().toString());
                    int numberOfGlobalNodes=Integer.valueOf(tvNGN.getText().toString());
                    int numberOfLocalNodes=Integer.valueOf(tvNLN.getText().toString());
                    int AmbientT=Integer.valueOf(tvAmbientT.getText().toString());

                    mesh=new Mesh(numberOfGlobalNodes, numberOfGlobalElements,  numberOfLocalNodes);
                    Mesh.setAmbientT(AmbientT);
                }
                else if(arrayLine[0].equals("e")){
                    if(elementIndex > 2) {
                        elementIndex=0;
                    }
                    int IdElement=Integer.valueOf(arrayLine[1]);
                    mesh.getElement(elementIndex).setIdElement(IdElement);
                    int localNumberOfNodes=mesh.getElement(elementIndex).getNumberOfNodes();
                    for(int nodeIndex=0; nodeIndex < localNumberOfNodes; nodeIndex++){
                        int idNode=Integer.valueOf(arrayLine[nodeIndex+2]);
                        mesh.getElement(elementIndex).getNodes(nodeIndex).setIdNode(idNode);
                        endNode=nodeIndex+2;
                    }
                    endNode++;

                    int alfa12=Integer.valueOf(arrayLine[endNode]);
                    int alfa23=Integer.valueOf(arrayLine[endNode+1]);
                    int alfa31=Integer.valueOf(arrayLine[endNode+2]);
                    int q12=Integer.valueOf(arrayLine[endNode+3]);
                    int q23=Integer.valueOf(arrayLine[endNode+4]);
                    int q31=Integer.valueOf(arrayLine[endNode+5]);
                    int K=Integer.valueOf(arrayLine[endNode+6]);

                    mesh.getElement(elementIndex).setAlfa12(alfa12);
                    mesh.getElement(elementIndex).setAlfa23(alfa23);
                    mesh.getElement(elementIndex).setAlfa31(alfa31);
                    mesh.getElement(elementIndex).setQ12(q12);
                    mesh.getElement(elementIndex).setQ23(q23);
                    mesh.getElement(elementIndex).setQ31(q31);
                    mesh.getElement(elementIndex).setK(K);

                    elementIndex++;
                }

                else if(arrayLine[0].equals("n")){
                    int numberOfElements=mesh.getGlobalNumberOfElements();
                    for(int Index=0;Index<numberOfElements; Index++){
                        int numberOfNodes=mesh.getElement(Index).getNumberOfNodes();
                        for(int nodeIndex=0;nodeIndex<numberOfNodes;nodeIndex++){
                            if(mesh.getElement(Index).getNodes(nodeIndex).getIdNode()==Integer.valueOf(arrayLine[1])){
                                int X=Integer.valueOf(arrayLine[2]);
                                int Y=Integer.valueOf(arrayLine[3]);
                                int BC=Integer.valueOf(arrayLine[4]);
                                mesh.getElement(Index).getNodes(nodeIndex).setX(X);
                                mesh.getElement(Index).getNodes(nodeIndex).setY(Y);
                                if(BC==0){
                                    mesh.getElement(Index).getNodes(nodeIndex).setBCState(com.example.lordkopakos.mes.mesh.Node.BoundaryCondition.NONE);
                                }
                                else if(BC==1){
                                    mesh.getElement(Index).getNodes(nodeIndex).setBCState(com.example.lordkopakos.mes.mesh.Node.BoundaryCondition.STREAM);
                                }
                                else if(BC==2){
                                    mesh.getElement(Index).getNodes(nodeIndex).setBCState(com.example.lordkopakos.mes.mesh.Node.BoundaryCondition.CONVECTION);
                                }
                                else{
                                    mesh.getElement(Index).getNodes(nodeIndex).setBCState(com.example.lordkopakos.mes.mesh.Node.BoundaryCondition.NONE);
                                }

                            }
                        }
                    }
                }

            }
        } catch (IOException e) {

        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
    }

    private void addHeaderToNodesInformation(){
        nodesItems.add("ID");
        nodesItems.add("X");
        nodesItems.add("Y");
        nodesItems.add("BC");
    }

    private void addHeaderToElementsInformation(){
        elementItems.add("ID");
        elementItems.add("Nod");
        elementItems.add("Alfa");
        elementItems.add("Q");
        elementItems.add("A");
        elementItems.add("K");
    }


    private void setElementInformation(int elementIndex){

        String IdElement=Integer.toString(mesh.getElement(elementIndex).getIdElement());
        String nodes="";

        int numberOfLocalNodes=mesh.getElement(elementIndex).getNumberOfNodes();
        for(int nodeIndex=0; nodeIndex<numberOfLocalNodes;nodeIndex++){
            nodes=nodes+Integer.toString(mesh.getElement(elementIndex).getNodes(nodeIndex).getIdNode())+"\n";

            //Nodes inforamtion
            String IdNodes=Integer.toString(mesh.getElement(elementIndex).getNodes(nodeIndex).getIdNode());
            String X=Float.toString(mesh.getElement(elementIndex).getNodes(nodeIndex).getX());
            String Y=Float.toString(mesh.getElement(elementIndex).getNodes(nodeIndex).getY());
            String BCName="";
            Node.BoundaryCondition BC=mesh.getElement(elementIndex).getNodes(nodeIndex).getBCState();
            if(BC== Node.BoundaryCondition.NONE){
                BCName="NONE";
            }
            else if(BC== Node.BoundaryCondition.STREAM){
                BCName="STREAM";
            }
            else if(BC==Node.BoundaryCondition.CONVECTION){
                BCName="CONVECTION";
            }
            else{
                BCName="ERROR";
            }
            nodesItems.add(IdNodes);
            nodesItems.add(X);
            nodesItems.add(Y);
            nodesItems.add(BCName);
        }

        String alfa=Float.toString(mesh.getElement(elementIndex).getAlfa12())+"\n"+Float.toString(mesh.getElement(elementIndex).getAlfa23())+"\n"+Float.toString(mesh.getElement(elementIndex).getAlfa31())+"\n";
        String Q=Float.toString(mesh.getElement(elementIndex).getQ12())+"\n"+Float.toString(mesh.getElement(elementIndex).getQ23())+"\n"+Float.toString(mesh.getElement(elementIndex).getQ31())+"\n";
        String A=Float.toString(mesh.getElement(elementIndex).getA());
        String K=Float.toString(mesh.getElement(elementIndex).getK());

        elementItems.add(IdElement);
        elementItems.add(nodes);
        elementItems.add(alfa);
        elementItems.add(Q);
        elementItems.add(A);
        elementItems.add(K);

    }

    public void solve(View view){
        mesh.calculateMesh();


        String temperatures = "";
        int numberOfGlobalNodes=mesh.getGlobalNumberOfNodes();
        for(int nodeIndex=0; nodeIndex<numberOfGlobalNodes;nodeIndex++){
            temperatures=temperatures+mesh.getTemperature(nodeIndex)+";";
        }
        Intent intent = new Intent(MeshInformationActivity.this, TemperaturesPresentationActivity.class);
        intent.putExtra("temperatures", temperatures);
        startActivity(intent);
    }
}
