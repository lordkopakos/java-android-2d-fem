package com.example.lordkopakos.mes.mesh;

/**
 * Created by lordkopakos on 2017-02-12.
 */
public class Node {
    private float t;

    private int idNode;
    private float x;
    private float y;

    BoundaryCondition BCState;

    public enum BoundaryCondition {
        NONE,
        STREAM,
        CONVECTION
    };

    Node(){
        setBCState(BoundaryCondition.NONE);
        setT(0);
    }
    Node(BoundaryCondition bc){
        setBCState(bc);
        setT(0);
    }
    Node(float t, BoundaryCondition bc){
        setBCState(bc);
        setT(t);
    }

    //Getters
    public float getT() {
        return t;
    }

    public int getIdNode() {
        return idNode;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public BoundaryCondition getBCState() {
        return BCState;
    }

    //Setters


    public void setT(float t) {
        this.t = t;
    }

    public void setIdNode(int idNode) {
        this.idNode = idNode;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setBCState(BoundaryCondition BCState) {
        this.BCState = BCState;
    }
}
