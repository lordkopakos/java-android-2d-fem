package com.example.lordkopakos.mes.IO;

import android.support.v7.app.AppCompatActivity;

import java.io.File;
import java.io.IOException;

/**
 * Created by lordkopakos on 2017-02-11.
 */
public class MyFile extends AppCompatActivity {

    File file;

    private String filePath;
    private String fileName;
    private String fileEnlargement;

    //Constructors
    //////////////////////////////////////////////////////////
    public MyFile(){
        setFileName("file");
        setFilePath("meshFile/");
        setFileEnlargement(".txt");
    }

    public MyFile(String filePath, String fileName,  String fileEnlargement){

        setFilePath(filePath);
        setFileName(fileName);
        setFileEnlargement(fileEnlargement);
    }
    //////////////////////////////////////////////////////////

    //Getters
    //////////////////////////////////////////////////////////
    public String getFileName() {
        return fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getFileEnlargement() {
        return fileEnlargement;
    }
    //////////////////////////////////////////////////////////

    //Setters
    //////////////////////////////////////////////////////////
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void setFileEnlargement(String fileEnlargement) {
        this.fileEnlargement = fileEnlargement;
    }
    //////////////////////////////////////////////////////////

    //Methods
    //////////////////////////////////////////////////////////
    private void createFileIfNotExist(){
        if(!checkFileExist(this.filePath, this.fileName, this.fileEnlargement)){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkFileExist(String filePath, String fileName, String fileEnlargement) {
        file = new File(filePath + fileName + fileEnlargement);
        return file.isFile();
    }
}
