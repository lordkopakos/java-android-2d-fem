package com.example.lordkopakos.mes.mesh;

import com.example.lordkopakos.mes.math.Math;

/**
 * Created by lordkopakos on 2017-02-12.
 */
public class Element {

    int idElement;

    //tablica węzłów
    int numberOfNodes;
    Node nodes[];

    //długość elementu
    float L12;
    float L23;
    float L31;

    //alfa
    float alfa12;
    float alfa23;
    float alfa31;

    //strumień ciepła
    float q12;
    float q23;
    float q31;

    //pole elementu
    float A;
    //współczynnik przewodzenia ciepła
    float K;

    //macierze lokalne
    float LH[][];
    float LP[];

    //Funkcje kształtu
    //float Ni;
    //float Nj;
    //float Nk;

    //Pochodne funkcji kształtu po X
    float dNidx;
    float dNjdx;
    float dNkdx;

    //Pochodne funkcji kształtu po Y
    float dNidy;
    float dNjdy;
    float dNkdy;

    //Zmienne potrzebne do wyliczenia funkcji kształtu
    float ai, bi, ci;
    float aj, bj, cj;
    float ak, bk, ck;


    //Constructors
    Element(){
        createNodes(3);
        createLocalMatrixH();
        createLocalVectorP();
    }
    Element(int numberOfNodes,int idElement){
        this.idElement = idElement;
        createNodes(numberOfNodes);
        createLocalMatrixH();
        createLocalVectorP();
    }

    public float getLHValue(int widthIndex, int heightIndex) {
        return LH[widthIndex][heightIndex];
    }

    public float getLPValue(int index){
        return LP[index];
    }

    //Getters
    public Node getNodes(int index) {
        return nodes[index];
    }

    public int getNumberOfNodes() {
        return numberOfNodes;
    }

    public float getK() {
        return K;
    }

    public int getIdElement() {
        return idElement;
    }

    public float getAlfa12() {
        return alfa12;
    }

    public float getAlfa23() {
        return alfa23;
    }

    public float getAlfa31() {
        return alfa31;
    }

    public float getL12() {
        return L12;
    }

    public float getL23() {
        return L23;
    }

    public float getL31() {
        return L31;
    }

    public float getQ12() {
        return q12;
    }

    public float getQ23() {
        return q23;
    }

    public float getQ31() {
        return q31;
    }

    public float getA() {
        return A;
    }

    //Setters
    public void setK(float k) {
        this.K = k;
    }

    public void setAlfa12(float alfa12) {
        this.alfa12 = alfa12;
    }

    public void setAlfa23(float alfa23) {
        this.alfa23 = alfa23;
    }

    public void setAlfa31(float alfa31) {
        this.alfa31 = alfa31;
    }

    public void setQ12(float q12) {
        this.q12 = q12;
    }

    public void setQ23(float q23) {
        this.q23 = q23;
    }

    public void setQ31(float q31) {
        this.q31 = q31;
    }

    public void setIdElement(int idElement) {
        this.idElement = idElement;
    }

    //Methods
    private void createNodes(int numberOfNodes){
        this.numberOfNodes=numberOfNodes;
        this.nodes=new Node[numberOfNodes];
        for(int nodesIndex=0; nodesIndex < numberOfNodes; nodesIndex++){
            this.nodes[nodesIndex]=new Node();
        }
    }

    private void createLocalMatrixH(){
        LH=new float[this.numberOfNodes][this.numberOfNodes];
        resetLocalMatrixH();
    }

    private void resetLocalMatrixH(){
        for(int indexWidth=0; indexWidth<this.numberOfNodes;indexWidth++){
            for(int indexHeight=0; indexHeight<this.numberOfNodes;indexHeight++){
                this.LH[indexWidth][indexHeight]=0;
            }
        }
    }

    private void createLocalVectorP(){
        LP=new float[this.numberOfNodes];
        resetLocalVectorP();
    }

    private void resetLocalVectorP(){
        for(int indexWidth=0; indexWidth<this.numberOfNodes;indexWidth++){
            this.LP[indexWidth]=0;
        }
    }

    public void initElement(){
        calculateFieldA();
        calculateAllL();
        calculateAllABC();
        calculateAllDerivativesShapeFunctionsN();
        calculateLocalMatrixH();
        addBoundaryConditionToLocalMatrixH();
        calculateLocalVectorP();
    }

    private void calculateFieldA(){
        this.A=((this.nodes[1].getX()-this.nodes[0].getX())*(this.nodes[2].getY()-this.nodes[0].getY())
                -(this.nodes[1].getY()-this.nodes[0].getY())*(this.nodes[2].getX()-this.nodes[0].getX()))/2;
    }

    private void calculateAllL(){
        calculateL12();
        calculateL23();
        calculateL31();
    }

    private void calculateL12(){
        this.L12= Math.calculateDistanceBetweenPoints(nodes[0], nodes[1]);
    }

    private void calculateL23(){
        this.L23=Math.calculateDistanceBetweenPoints(nodes[1], nodes[2]);
    }

    private void calculateL31(){
        this.L31=Math.calculateDistanceBetweenPoints(nodes[2], nodes[0]);
    }

    private void calculateAllABC(){
        calculateAiBiCi();
        calculateAjBjCj();
        calculateAkBkCk();
    }

    private void calculateAiBiCi(){
        this.ai=nodes[1].getX()*nodes[2].getY()-nodes[2].getX()*nodes[1].getY();
        this.bi=nodes[1].getY()-nodes[2].getY();
        this.ci=nodes[2].getX()-nodes[1].getX();
    }

    private void calculateAjBjCj(){
        this.aj=nodes[2].getX()*nodes[0].getY()-nodes[0].getX()*nodes[2].getY();
        this.bj=nodes[2].getY()-nodes[0].getY();
        this.cj=nodes[0].getX()-nodes[2].getX();
    }

    private void calculateAkBkCk(){
        this.ak=nodes[0].getX()*nodes[1].getY()-nodes[1].getX()*nodes[0].getY();
        this.bk=nodes[0].getY()-nodes[1].getY();
        this.ck=nodes[1].getX()-nodes[0].getX();
    }

    private void calculateAllDerivativesShapeFunctionsN(){
        calculateDNiDX();
        calculateDNjDX();
        calculateDNkDX();
        calculateDNiDY();
        calculateDNjDY();
        calculateDNkDY();
    }

    private void calculateDNiDX(){
        this.dNidx=(this.bi)/2*A;
    }

    private void calculateDNjDX(){
        this.dNjdx=(this.bj)/2*A;
    }

    private void calculateDNkDX(){
        this.dNkdx=(this.bk)/2*A;
    }

    private void calculateDNiDY(){
        this.dNidy=(this.bi)/2*A;
    }

    private void calculateDNjDY(){
        this.dNjdy=(this.bj)/2*A;
    }

    private void calculateDNkDY(){
        this.dNkdy=(this.bk)/2*A;
    }

    private void calculateLocalMatrixH(){
        this.LH[0][0]=this.K*(this.dNidx*this.dNidx+this.dNidy*this.dNidy)*this.A;
        this.LH[0][1]=this.K*(this.dNidx*this.dNjdx+this.dNidy*this.dNjdy)*this.A;
        this.LH[0][2]=this.K*(this.dNidx*this.dNkdx+this.dNidy*this.dNkdy)*this.A;
        this.LH[1][0]=this.K*(this.dNjdx*this.dNidx+this.dNjdy*this.dNidy)*this.A;
        this.LH[1][1]=this.K*(this.dNjdx*this.dNjdx+this.dNjdy*this.dNjdy)*this.A;
        this.LH[1][2]=this.K*(this.dNjdx*this.dNkdx+this.dNjdy*this.dNkdy)*this.A;
        this.LH[2][0]=this.K*(this.dNkdx*this.dNidx+this.dNkdy*this.dNidy)*this.A;
        this.LH[2][1]=this.K*(this.dNkdx*this.dNjdx+this.dNkdy*this.dNjdy)*this.A;
        this.LH[2][2]=this.K*(this.dNkdx*this.dNkdx+this.dNkdy*this.dNkdy)*this.A;
    }

    private void addBoundaryConditionToLocalMatrixH(){
        if(checkBoundaryConditions()){
            float[][] Hbc1=new float[numberOfNodes][numberOfNodes];
            float[][] Hbc2=new float[numberOfNodes][numberOfNodes];
            float[][] Hbc3=new float[numberOfNodes][numberOfNodes];

            float C1=alfa12*L12/6;
            float C2=alfa23*L23/6;
            float C3=alfa31*L31/6;

            Hbc1=Math.resetMatrix(numberOfNodes, numberOfNodes);
            Hbc2=Math.resetMatrix(numberOfNodes, numberOfNodes);
            Hbc3=Math.resetMatrix(numberOfNodes, numberOfNodes);

            Hbc1[0][0]=2;
            Hbc1[0][1]=1;
            Hbc1[1][0]=2;
            Hbc1[1][1]=1;

            Hbc2[1][1]=2;
            Hbc2[1][2]=1;
            Hbc2[2][1]=2;
            Hbc2[2][2]=1;

            Hbc3[0][0]=2;
            Hbc3[2][0]=1;
            Hbc3[0][2]=2;
            Hbc3[2][2]=1;

            Hbc1=Math.multipyMatrixByNumber(Hbc1, numberOfNodes, numberOfNodes,C1);
            Hbc2=Math.multipyMatrixByNumber(Hbc2, numberOfNodes, numberOfNodes,C2);
            Hbc3=Math.multipyMatrixByNumber(Hbc3, numberOfNodes, numberOfNodes,C3);

            Hbc1=Math.addTwoMatrixs(Hbc1,Hbc2, numberOfNodes, numberOfNodes);
            Hbc1=Math.addTwoMatrixs(Hbc1,Hbc3, numberOfNodes, numberOfNodes);

            this.LH=Math.addTwoMatrixs(this.LH,Hbc1, numberOfNodes, numberOfNodes);
        }
    }

    private boolean checkBoundaryConditions(){
        for(int nodeIndex=0; nodeIndex<numberOfNodes; nodeIndex++){
            if(nodes[nodeIndex].getBCState()== Node.BoundaryCondition.CONVECTION){
                return true;
            }
        }
        return false;
    }

    private void calculateLocalVectorP(){
        if(checkBoundaryConditions()){
            Math.resetVector(numberOfNodes);
            float Pbc1[]= new float[this.numberOfNodes];
            float Pbc2[]= new float[this.numberOfNodes];
            float Pbc3[]= new float[this.numberOfNodes];

            Pbc1[0]=1;
            Pbc1[1]=1;

            Pbc2[1]=1;
            Pbc2[2]=1;

            Pbc3[0]=1;
            Pbc3[2]=1;

            float C1=(alfa12*Mesh.getAmbientT()+q12)*L12/2;
            float C2=(alfa23*Mesh.getAmbientT()+q23)*L23/2;
            float C3=(alfa31*Mesh.getAmbientT()+q31)*L31/2;

            Pbc1=Math.multipyVectorByNumber(Pbc1, numberOfNodes, C1);
            Pbc2=Math.multipyVectorByNumber(Pbc2, numberOfNodes, C2);
            Pbc3=Math.multipyVectorByNumber(Pbc3, numberOfNodes, C3);

            Pbc1=Math.addTwoVectors(Pbc1, Pbc2, numberOfNodes);
            Pbc1=Math.addTwoVectors(Pbc1, Pbc3, numberOfNodes);

            this.LP=Pbc1;
        }
        else{
            Math.resetVector(numberOfNodes);
        }
    }

}
