package com.example.lordkopakos.mes.math;

import com.example.lordkopakos.mes.mesh.Node;

/**
 * Created by lordkopakos on 2017-02-13.
 */
public class Math {
    public static float pow(float what,float power){
        float result=1;
        for(int i=0; i<power;i++){
            result*=what;
        }
        return result;
    }

    public static float calculateDistanceBetweenPoints(Node firstNode, Node secondNode){
        double distance= java.lang.Math.sqrt(java.lang.Math.pow(secondNode.getX()-firstNode.getX(),2)+java.lang.Math.pow(secondNode.getY()-firstNode.getY(),2));;
        return (float)distance;
    }

    public static float[][] addTwoMatrixs(float[][] firstMatrix, float [][] secondMatrix, int width, int height){
        float resultMatrix[][]=new float[width][height];
        for(int widthIndex=0; widthIndex<width; widthIndex++){
            for(int heightIndex=0; heightIndex<height; heightIndex++){
                resultMatrix[widthIndex][heightIndex]=firstMatrix[widthIndex][heightIndex]+secondMatrix[widthIndex][heightIndex];
            }
        }
        return resultMatrix;
    }

    public static float[][] multipyMatrixByNumber(float[][] matrix, int width, int height, float number){
        float resultMatrix[][]=new float[width][height];
        for(int widthIndex=0; widthIndex<width; widthIndex++){
            for(int heightIndex=0; heightIndex<height; heightIndex++){
                resultMatrix[widthIndex][heightIndex]=number*matrix[widthIndex][heightIndex];
            }
        }
        return resultMatrix;
    }

    public static float[][] resetMatrix(int width, int height){
        float[][] matrix=new float[width][height];
        for(int indexWidth=0; indexWidth<width;indexWidth++){
            for(int indexHeight=0; indexHeight<height;indexHeight++){
                matrix[indexWidth][indexHeight]=0;
            }
        }
        return matrix;
    }

    public static float[] resetVector(int width){
        float[] vector=new float[width];
        for(int indexWidth=0; indexWidth<width;indexWidth++){
            vector[indexWidth]=0;
        }
        return vector;
    }

    public static float[] addTwoVectors(float[] firstVector, float [] secondVector, int width){
        float resultVector[]=new float[width];
        for(int widthIndex=0; widthIndex<width; widthIndex++){
            resultVector[widthIndex]=firstVector[widthIndex]+secondVector[widthIndex];
        }
        return resultVector;
    }

    public static float[] multipyVectorByNumber(float[] vector, int width, float number){
        float resultVector[]=new float[width];
        for(int widthIndex=0; widthIndex<width; widthIndex++){
            resultVector[widthIndex]=number*vector[widthIndex];
        }
        return resultVector;
    }


}
